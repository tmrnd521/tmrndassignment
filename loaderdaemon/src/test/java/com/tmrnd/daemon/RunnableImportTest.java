package com.tmrnd.daemon;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Test;;

/**
 * Unit test for Each of the file processing: implemented class RunnableImport.
 */
public class RunnableImportTest {

    @Test
    public void testPasingTask() {
    	Path pTask = Paths.get("F:\\loaderdaemon\\daemon\\files\\task.csv");
    	RunnableImport rImp1 = new RunnableImport("Thread-1", pTask);
        rImp1.start();
        
    }
}
