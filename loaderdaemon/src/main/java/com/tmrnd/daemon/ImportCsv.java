package com.tmrnd.daemon;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.opencsv.CSVReader;

public class ImportCsv {

	private CSVReader reader = null;
	private Connection conn = null;
	private DBUtil dBUtil = null;
	
	public String filename;
	
	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}


	private void Initilize () {
		try {
	    	//sample filename format: "task.csv"
	    	reader = new CSVReader(new FileReader(filename), ',');
	    	dBUtil = new DBUtil();
	    	conn =  dBUtil.getConnection();
	    	
		} catch(Exception e) {
            e.printStackTrace();
        }
	}
	
	private void Dispose() throws SQLException {
		if (reader!= null) {
			try {
				reader.close();
			} catch (IOException e) { }
		}
		dBUtil.disConnect(conn);
	}

    public void importAllCsv() throws SQLException
    {
    	this.Initilize();
    	
    	if (filename.contains("task.csv"))
        	importTask();
    	else if (filename.contains("team.csv"))
    		importTeam();
    	else if (filename.contains("team_skill.csv"))
    		importTeamSkill();
    	
		this.Dispose();
    	
    }
    
    private void importTask()
    {
        try {
            String insertQuery = "Insert into task (task_id, skill) values (?, ?)";
            PreparedStatement pstmt = conn.prepareStatement(insertQuery);
			String[] rowData = null;
			int i = 0;
			while((rowData = reader.readNext()) != null) {
				i++;
				//escape to parse the first line!
				if (i == 1)
					continue;
				
				pstmt.setString(1, rowData[0]);
				pstmt.setString(2, rowData[1]);
				pstmt.execute();
				
			}
			
			System.out.println("Successfully Uploaded task");
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    private void importTeam()
    {
        try {
            String insertQuery = "Insert into team (team_id) values (?)";
            PreparedStatement pstmt = conn.prepareStatement(insertQuery);
			String[] rowData = null;
			int i = 0;
			while((rowData = reader.readNext()) != null) {
				i++;
				//escape to parse the first line!
				if (i == 1)
					continue;
				
				pstmt.setString(1, rowData[0]);
				pstmt.execute();
				
			}
			
			System.out.println("Successfully Uploaded team");
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    private void importTeamSkill()
    {
        try {
            String insertQuery = "Insert into team_skill (team_id, skill) values (?, ?)";
            PreparedStatement pstmt = conn.prepareStatement(insertQuery);
			String[] rowData = null;
			int i = 0;
			while((rowData = reader.readNext()) != null) {
				i++;
				//escape to parse the first line!
				if (i == 1)
					continue;
				
				pstmt.setString(1, rowData[0]);
				pstmt.setString(2, rowData[1]);
				pstmt.execute();
				
			}
			
			System.out.println("Successfully Uploaded team_skill");
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

}
