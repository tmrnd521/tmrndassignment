package com.tmrnd.daemon;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBUtil {
	private static String jdbcUrl = "jdbc:mysql://localhost:3306/tmrnd?useTimezone=true&serverTimezone=UTC";
	private static String userName = "root";
	private static String password = "zhaohl@2019!";
	
	private Connection conn;

 
    public Connection getConnection() throws SQLException, ClassNotFoundException
    {
    	try {
    		//Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
    		
    		conn = DriverManager.getConnection(jdbcUrl, userName, password);
 
    	} catch (Exception e) {
    		e.printStackTrace();
            conn = null;
    	}
    	
    	return conn;
    }
    
    public void disConnect(Connection conn) throws SQLException {
    	try {
	    	if (conn != null && !conn.isClosed()) {
				conn.close();
	    	}
    	} catch (SQLException e) {
     		e.printStackTrace();
     	} finally {
     		conn = null;
		}
    }

}
