package com.tmrnd.daemon;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

/**
 * Watch Service!
 *
 */
public class TMRNDWatchService 
{
    public static void main( String[] args ) throws IOException
    {
        System.out.println( "Program Start!" );
        
        Path dir = Paths.get("./files/");
        
        WatchService watchService = FileSystems.getDefault().newWatchService();
        dir.register(watchService, StandardWatchEventKinds.ENTRY_CREATE);
        
        /*dir.register(watchService, StandardWatchEventKinds.ENTRY_CREATE,
        		StandardWatchEventKinds.ENTRY_DELETE,
        		StandardWatchEventKinds.ENTRY_MODIFY);*/
        
        
        // Processing Events
        for (;;) {

            // wait for key to be signaled
            WatchKey key;
            try {
                key = watchService.take();
            } catch (InterruptedException x) {
                return;
            }

            for (WatchEvent<?> event: key.pollEvents()) {
                WatchEvent.Kind<?> kind = event.kind();

                // This key is registered only for ENTRY_CREATE events
                if (kind == StandardWatchEventKinds.OVERFLOW) {
                    continue;
                }

                // The filename is the context of the event.
                WatchEvent<Path> ev = (WatchEvent<Path>)event;
                Path filename = ev.context();

                // Verify that the new file is a text file.
                try {
                    // Resolve the filename against the directory.
                    Path child = dir.resolve(filename);
                    
                    // print out event
                    //System.out.format("%s: %s\n", event.kind().name(), child);
                    
                    //System.out.println(Files.probeContentType(child));
                    if (!Files.probeContentType(child).equals("application/vnd.ms-excel")) {
                        System.err.format("New file '%s'" +
                            " is not a csv file.%n", filename);
                        continue;
                    }
                    
                    // Load data from the csv files to MySQL database
                    //System.out.format("Locad data from the csv files to MySQL database %s%n", child);
                    
                    // DIRECTLY IMPORT
                    /* ImportCsv ic = new ImportCsv();
                    ic.setFilename(child.toString());
                    ic.importAllCsv(); */
                    
                    // USING THREADS TO OPTIMIZE PERFORMANCE
                    String cfilename = child.toString();
                	if (cfilename.contains("task.csv")) {
                        RunnableImport rImp1 = new RunnableImport("Thread-1", child);
                        rImp1.start();
                	} else if (cfilename.contains("team.csv")) {
                		RunnableImport rImp2 = new RunnableImport("Thread-2", child);
                		rImp2.start();
                	}
                	else if (cfilename.contains("team_skill.csv")) {
                		RunnableImport rImp3 = new RunnableImport("Thread-3", child);
                		rImp3.start();
                	}
                    
                } catch (IOException x) {
                    System.err.println(x);
                    continue;
                }
                
            }

            // Reset the key -- this step is critical if you want to receive further watch events.
            // If the key is no longer valid, the directory is inaccessible so exit the loop.
            boolean valid = key.reset();
            if (!valid) {
                break;
            }
        }
        
    }
}
