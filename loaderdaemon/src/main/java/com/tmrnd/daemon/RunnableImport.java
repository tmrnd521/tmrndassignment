package com.tmrnd.daemon;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.SQLException;

public class RunnableImport implements Runnable {

	private Thread t;
	private String threadname;
	private Path filename;
	
	RunnableImport( String tname, Path fname) {
		threadname = tname;
		filename = fname;
		System.out.println(threadname + ": Processing " +  filename );
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		System.out.println("Running " +  threadname );
	    try {
	    	ImportCsv ic = new ImportCsv();
            ic.setFilename(filename.toString());
            ic.importAllCsv();
            // Let the thread sleep for a while.
            Thread.sleep(50);
            
	    } catch (SQLException e) {
	    	System.out.println("Thread " +  threadname + " interrupted: SQL Exception.");
	    }
	    catch (InterruptedException e) {
	         System.out.println("Thread " +  threadname + " interrupted.");
	    }
	    System.out.println("Thread " +  threadname + " finished.");
	    
    	// Delete the csv file
	    try {
	    	Files.delete(filename);
	    } catch (IOException e) {
	    	System.err.println(e);
	    }
    	System.out.println(filename + " have beeen deleted Successfully!");
	}
	
	public void start () {
		System.out.println("Starting " +  threadname );
		if (t == null) {
			t = new Thread (this, threadname);
			t.start ();
		}
	}

}
