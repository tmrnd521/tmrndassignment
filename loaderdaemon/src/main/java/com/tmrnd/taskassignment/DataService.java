package com.tmrnd.taskassignment;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.tmrnd.daemon.DBUtil;

public class DataService {

	private Connection conn = null;
	private DBUtil dBUtil = null;
	
	
	private void Initilize () {
		try {
			dBUtil = new DBUtil();
	    	conn =  dBUtil.getConnection();
	    	
		} catch(Exception e) {
            e.printStackTrace();
        }
	}
	
	private void Dispose() throws SQLException {
		dBUtil.disConnect(conn);
	}
	
    public List<TaskModel> getTaskList()
    {
    	List<TaskModel> taskList = null;
        try
        {
        	this.Initilize();
        	
        	String query = "SELECT task_id, skill FROM task ORDER BY task_id, skill;";
        	
        	PreparedStatement pstmt = conn.prepareStatement(query);
        	
        	ResultSet rs = pstmt.executeQuery(query);

        	taskList = new ArrayList<TaskModel>();
        	while (rs.next())
            {
        		TaskModel model = new TaskModel();
        		String taskId = rs.getString("task_id");
        		String skill = rs.getString("skill");
        		model.setTaskId(taskId);
        		model.setSkill(skill);
        		taskList.add(model);
            }
        	pstmt.close();
        	
        	this.Dispose();
        	
        } catch (Exception e) {
        	System.err.println(e.getMessage());
        }
        
        return taskList;
    }
	
    public List<TeamSkillModel> getTeamSkillList(String paraSkill)
    {
    	List<TeamSkillModel> tsList = null;
        try
        {
        	this.Initilize();
        	
        	String query = "SELECT team_id, skill FROM team_skill WHERE skill = '" + paraSkill + "' ORDER BY team_id, skill;";
        	
        	PreparedStatement pstmt = conn.prepareStatement(query);
        	//pstmt.setString(1, paraSkill);
        	
        	ResultSet rs = pstmt.executeQuery(query);

        	tsList = new ArrayList<TeamSkillModel>();
        	while (rs.next())
            {
        		TeamSkillModel model = new TeamSkillModel();
        		String teamId = rs.getString("team_id");
        		String skill = rs.getString("skill");
        		model.setTeamId(teamId);
        		model.setSkill(skill);
        		tsList.add(model);
            }
        	pstmt.close();
        	
        	this.Dispose();
        	
        } catch (Exception e) {
        	System.err.println(e.getMessage());
        }
        
        return tsList;
    }

}
