package com.tmrnd.taskassignment;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

public class AssignTask {
	
	// To indicate concurrent task count assigned to TEAMS
	int globalSize = 0;
	// To store assigned tasks for all the teams
	List<TaskModel> assignedTask1 = new ArrayList<TaskModel>();
	List<TaskModel> assignedTask2 = new ArrayList<TaskModel>();
	List<TaskModel> assignedTask3 = new ArrayList<TaskModel>();
	List<TaskModel> assignedTask4 = new ArrayList<TaskModel>();
	List<TaskModel> assignedTask5 = new ArrayList<TaskModel>();
	// Remaining task list
	List<TaskModel> remainingTaskList = new ArrayList<TaskModel>();
	
	
	public String distributeTask() {
		DataService service = new DataService();
		List<TaskModel> taskList = service.getTaskList();
		
		for (int i=0; i<taskList.size(); i++) {
			TaskModel model = taskList.get(i);
			
			//Assign one task to a particular team
			assignOneTaskToAParticularTeam(service, model);
			
		}
		
		if (remainingTaskList != null && remainingTaskList.size() != 0) {
			if (remainingTaskList.size() > 1) {
				for (int i=0; i<remainingTaskList.size(); i++) {
					TaskModel model = remainingTaskList.get(i);
					
					//Assign one task to a particular team
					assignOneTaskToAParticularTeam(service, model);
					
				}
			} else {
				TaskModel model = remainingTaskList.get(0);
				
				//Distribute the last task
				assignTheLastTaskToATeam(model);
			}
			
		}
		
		// For testing purpose only!
		//this.PrintTasks();
		
		// Transform the Assigned task list into a JSON Object
		String jsonTasks = getAssignedTaskJSON();
		return jsonTasks;
	}
	
	private void assignOneTaskToAParticularTeam(DataService service, TaskModel model) {
		List<TeamSkillModel> tsList = null;
		boolean isAssigned = false;
		
		String skill = model.getSkill();
		//This is a qualified team list
		tsList = service.getTeamSkillList(skill);
		for (int i=0; i<tsList.size(); i++) {
			TeamSkillModel tsModel = tsList.get(i);
			String teamId = tsModel.getTeamId();
			
			if ("TEAM_01".equals(teamId) && (assignedTask1.size() == globalSize)) {
				assignedTask1.add(model);
				isAssigned = true;
				break;
			} else if ("TEAM_02".equals(teamId) && (assignedTask2.size() == globalSize)) {
				assignedTask2.add(model);
				isAssigned = true;
				break;
			} else if ("TEAM_03".equals(teamId) && (assignedTask3.size() == globalSize)) {
				assignedTask3.add(model);
				isAssigned = true;
				break;
			} else if ("TEAM_04".equals(teamId) && (assignedTask4.size() == globalSize)) {
				assignedTask4.add(model);
				isAssigned = true;
				break;
			} else if ("TEAM_05".equals(teamId) && (assignedTask5.size() == globalSize)) {
				assignedTask5.add(model);
				isAssigned = true;
				break;
			} else {
				// COULD NOT BE ASSIGNED, continue loop for next team!
				continue;
			}
			
		}
		
		// If could not be assigned finally, then put the task into remaining task list.
		if (isAssigned == false) {
			remainingTaskList.add(model);
		}
		
		// Move up the global size of assigned task.
		if (assignedTask1.size() == assignedTask2.size()
				&& assignedTask2.size() == assignedTask3.size()
				&& assignedTask3.size() == assignedTask4.size()
				&& assignedTask4.size() == assignedTask5.size()) {
			globalSize++;
		}
		
		
	}
	
	private void assignTheLastTaskToATeam(TaskModel model) {
		
		// To get the team of which has the least tasks
		// Of which we should assign another task.
		if (assignedTask1.size() < globalSize) {
			TaskModel tModel = assignedTask2.get(0);
			assignedTask1.add(tModel);
			assignedTask2.add(model);
		} else if (assignedTask2.size() < globalSize) {
			TaskModel tModel = assignedTask3.get(0);
			assignedTask2.add(tModel);
			assignedTask3.add(model);
		} else if (assignedTask3.size() < globalSize) {
			TaskModel tModel = assignedTask4.get(0);
			assignedTask3.add(tModel);
			assignedTask4.add(model);
		} else if (assignedTask4.size() < globalSize) {
			TaskModel tModel = assignedTask5.get(0);
			assignedTask4.add(tModel);
			assignedTask5.add(model);
		} else if (assignedTask5.size() < globalSize) {
			TaskModel tModel = assignedTask1.get(0);
			assignedTask5.add(tModel);
			assignedTask1.add(model);
		}
		
		
	}
	
	private void PrintTasks() {
		for (int i= 0; i <assignedTask1.size(); i++) {
			TaskModel model = assignedTask1.get(i);
			System.out.println("Assigned Team 1: " + i + " | Task Id: " + model.getTaskId());
		}
		for (int i= 0; i <assignedTask2.size(); i++) {
			TaskModel model = assignedTask2.get(i);
			System.out.println("Assigned Team 2: " + i + " | Task Id: " + model.getTaskId());
		}
		for (int i= 0; i <assignedTask3.size(); i++) {
			TaskModel model = assignedTask3.get(i);
			System.out.println("Assigned Team 3: " + i + " | Task Id: " + model.getTaskId());
		}
		for (int i= 0; i <assignedTask4.size(); i++) {
			TaskModel model = assignedTask4.get(i);
			System.out.println("Assigned Team 4: " + i + " | Task Id: " + model.getTaskId());
		}
		for (int i= 0; i <assignedTask5.size(); i++) {
			TaskModel model = assignedTask5.get(i);
			System.out.println("Assigned Team 5: " + i + " | Task Id: " + model.getTaskId());
		}
	}
	
	private String getAssignedTaskJSON () {
		JSONObject jsonTasks = new JSONObject();
		String jsonString = "{";
		String jsonString1 = "\"TEAM_01\":{";
		String jsonString2 = "\"TEAM_02\":{";
		String jsonString3 = "\"TEAM_03\":{";
		String jsonString4 = "\"TEAM_04\":{";
		String jsonString5 = "\"TEAM_05\":{";
		
		for (int i= 0; i <assignedTask1.size(); i++) {
			TaskModel model = assignedTask1.get(i);
			jsonString1 += "\"" + model.getTaskId() + "\",";
		}
		for (int i= 0; i <assignedTask2.size(); i++) {
			TaskModel model = assignedTask2.get(i);
			jsonString2 += "\"" + model.getTaskId() + "\",";
		}
		for (int i= 0; i <assignedTask3.size(); i++) {
			TaskModel model = assignedTask3.get(i);
			jsonString3 += "\"" + model.getTaskId() + "\",";
		}
		for (int i= 0; i <assignedTask4.size(); i++) {
			TaskModel model = assignedTask4.get(i);
			jsonString4 += "\"" + model.getTaskId() + "\",";
		}
		for (int i= 0; i <assignedTask5.size(); i++) {
			TaskModel model = assignedTask5.get(i);
			jsonString5 += "\"" + model.getTaskId() + "\",";
		}
		if (jsonString1.endsWith(",")) {
			jsonString1 = jsonString1.substring(0, jsonString1.length()-1) + "}";
		}
		if (jsonString2.endsWith(",")) {
			jsonString2 = jsonString2.substring(0, jsonString2.length()-1) + "}";
		}
		if (jsonString3.endsWith(",")) {
			jsonString3 = jsonString3.substring(0, jsonString3.length()-1) + "}";
		}
		if (jsonString4.endsWith(",")) {
			jsonString4 = jsonString4.substring(0, jsonString4.length()-1) + "}";
		}
		if (jsonString5.endsWith(",")) {
			jsonString5 = jsonString5.substring(0, jsonString5.length()-1) + "}";
		}
		
		jsonString += jsonString1 + "," + jsonString2 + "," + jsonString3 + "," + jsonString4 + "," + jsonString5 + "}";
		if (jsonString.endsWith(",")) {
			jsonString = jsonString.substring(0, jsonString.length()-1) + "}";
		}
		System.out.println(jsonString);
		
		//jsonTasks.getJSONObject(jsonString);
		
		return jsonString;
	}

}
