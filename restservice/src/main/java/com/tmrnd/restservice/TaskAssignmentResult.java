package com.tmrnd.restservice;

public class TaskAssignmentResult {

    private String taskId;
    private String teamId;

    public TaskAssignmentResult(String taskId, String teamId) {
        this.taskId = taskId;
        this.teamId = teamId;
    }

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getTeamId() {
		return teamId;
	}

	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}

}
