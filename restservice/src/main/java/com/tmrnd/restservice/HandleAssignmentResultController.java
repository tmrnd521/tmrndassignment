package com.tmrnd.restservice;

import java.util.HashMap;

import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HandleAssignmentResultController {
    
    @RequestMapping(value="/assignresult", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public Object handleAssignmentResult(@RequestBody TaskAssignmentResult result)
    {
        System.out.println(result.getTaskId() + " " + result.getTeamId());
        
        // To store assignment result data into Database.
        // HAVE not done yet. Because No time already!
    	
        return new HashMap<String, String>().put("success", "true");
    }
}
